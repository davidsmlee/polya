# This R-script contains utility functions that are used by multiple notebooks
# David Lee
# June 2019

#####################
## Data Processing ##
#####################
granges_from_vcf <- function(vcf) {
  
  vcf$end <- vcf$POS
  grange <- makeGRangesFromDataFrame(vcf,
                                     keep.extra.columns = TRUE,
                                     ignore.strand = TRUE,
                                     seqnames.field = "CHROM",
                                     start.field = "POS",
                                     end.field = "end")
  
  return(grange)
  
}



####################
## MAPS functions ##
####################
get_ps <- function(vcf_list, mu) {
  
  n <- lapply(vcf_list, function(x) table(x$mutation))
  ps <- lapply(vcf_list, function(x) table(x[x$AC == 1]$mutation))
  
  ps_res <- mapply(function(x, y) cbind(x, y[match(names(x), names(y))]), x=n, y=ps, SIMPLIFY = FALSE)
  ps_res <- lapply(ps_res, function(x) x[match(mu$mutation, rownames(x)),])
  
  return(ps_res)
}

get_mu_avg <- function(ps_list, mu) {
  
  res <- lapply(ps_list, function(x) sum((x*mu$mu_snp), na.rm = TRUE) / sum(x, na.rm = TRUE))
  
  
  return(res)
}

calc_maps <- function(raw_ps, ps_counts, predictions) {
  
  obs <- unlist(raw_ps) * unlist(ps_counts)
  exp <- unlist(predictions) * unlist(ps_counts)
  
  maps <- (obs - exp) / unlist(ps_counts)
  
  return(maps)
  
}

do_MAPS <- function(vcf_list, mu, ps_model) {
  
  # determine proportion singletons
  ps <- get_ps(vcf_list, mu)
  
  # determine mu average for the variants + contexts
  mu_avg <- get_mu_avg(ps, mu)
  
  # extract raw proportion of singletons
  raw_p <- lapply(vcf_list, function(x) length(x[x$AC==1])/length(x))
  var_counts <- lapply(vcf_list, function(x) length(x))
  se <- mapply(function(p, n ) sqrt(p*(1-p)/n), raw_p, var_counts)
  
  # predicted ps:
  ps_predict <- lapply(mu_avg, function(x) predict(ps_model, data.frame(mu = unlist(x))))
  
  maps <- calc_maps(raw_p, var_counts, ps_predict)
  
  return(maps)
  
}

##################################
## Variant Processing Functions ##
##################################

annotate_position <- function(grange, relative_end, relative_start, width = 3) {
  
  # this function accepts a grange object and annotates a position based on strand and provided width
  
  nt <- length(grange)
  
  if (nt %% 3 != 0) {
    
    positions <- rep('incorrect sequence lengths', nt)
    dist_from_end <- c(relative_end:relative_start)
    
  }
  
  if (as.character(strand(grange))[[1]] == '+') {
    
    # positive strand
    positions <- rep(1:width, nt/3)
    dist_from_end <- c(relative_end:relative_start)
    
  } else {
    
    positions <- rep(width:1, nt/3)
    dist_from_end <- c(relative_end:relative_start)
  }
  
  grange$position <- positions
  grange$dist_from_end <- dist_from_end
  
  return(grange)
  
}

tile_map_positions <- function(exons, fake_exons = FALSE) {
  
  # the purpose of this funciton is the tile genomic ranges and map their relative positions
  # within a putative protein-coding transcript
  split_df <- split(exons, exons$`ID:TX:CHR:STRAND:RELATIVE_START:RELATIVE_END`)
  if (fake_exons) {
    relative_end <- 600
    relative_start <- 1
  } else {
    relative_end <- as.numeric(gsub('.*:','',names(split_df)))
    relative_start <- as.numeric(sub(".*[+|-]: *(.*?) *:.*", "\\1", names(split_df)))  
  }
  
  granges <- lapply(split_df, function(x)  makeGRangesFromDataFrame(x, keep.extra.columns = TRUE,
                                                                    seqnames.field = 'CHROMOSOME',
                                                                    start.field = 'EXON_STARTS',
                                                                    end.field = 'EXON_ENDS',
                                                                    starts.in.df.are.0based = TRUE))
  granges <- lapply(granges, function(x) unlist(tile(x, width = 1)))
  granges <- mapply(x = granges, y = relative_end, z = relative_start, function(x,y,z) annotate_position(x,y,z))
  granges <- unlist(granges)
  granges <- lapply(granges, function(x) data.frame(x))
  
  granges_result <- data.table::rbindlist(granges, idcol = TRUE)
  
  return(granges_result)
  
}

get_rna_sequence <- function(exons) {
  
  heptamer <- exons$heptamer
  
  strand <- exons$strand
  
  
  heptamers <- mapply(function(heptamer, strand) ifelse(strand == '+',
                                                        heptamer,
                                                        chartr('ATGC','TACG',
                                                               paste(rev(substring(heptamer,
                                                                                   1:nchar(heptamer),
                                                                                   1:nchar(heptamer))),
                                                                     collapse = ''))), heptamer = heptamer, strand)
  exons$heptamer <- heptamers
  
  ref <- exons$REF
  alt <- exons$ALT
  
  RNA_ref <- mapply(function(ref, strand) ifelse(strand == '+',
                                                 ref,
                                                 chartr('ATGC','TACG', ref)),
                    ref = ref, strand = strand)
  
  RNA_alt <- mapply(function(alt, strand) ifelse(strand == '+',
                                                 alt,
                                                 chartr('ATGC','TACG', alt)),
                    alt = alt, strand = strand)
  
  
  exons$RNA_ref <- RNA_ref
  exons$RNA_alt <- RNA_alt
  
  
  return(exons)
  
}

get_codons <- function(exons) {
  
  
  heptamer <- exons$heptamer
  position <- exons$position
  
  codons <- mapply(function(heptamer, position) substr(heptamer, 5-position, 7-position),
                   heptamer = heptamer, position = position)
  
  exons$reference_codon <- codons
  
  alt_base <- exons$RNA_alt
  
  altmers <- mapply(function(heptamer, alt_base) paste0(substr(heptamer, 1, 3), alt_base, substr(heptamer, 5, 7)),
                    heptamer = heptamer, alt_base = alt_base)
  
  alt_codons <- mapply(function(altmers, position) substr(altmers, 5-position, 7-position),
                       altmers = altmers, position = position)
  
  exons$alt_codon <- alt_codons
  
  return(exons)
  
}

annotate_consequence <- function(ref_codon, alt_codon, dist_from_end) {
  
  if (ref_codon == alt_codon | (alt_codon == 'Stp' & dist_from_end <= 50)) {
    
    return('synonymous')
    
  } else if (alt_codon == 'Stp' | ref_codon == 'Stp') {
    
    return('stop_gain_loss')
    
  } else {
    
    return('missense')
    
  }
  
}


# load codons table
codons <- data.table::fread('../external_data/codon.txt')
codon_lookup <- codons$AminoAcid
names(codon_lookup) <- codons$Codon
