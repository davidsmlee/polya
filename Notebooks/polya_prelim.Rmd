---
title: "polya preliminary analysis"
output: html_notebook
---

This notebook contains exploratory anlysis of sequence constraint over polyadenylation sites.

Set up environment and load RData objects
```{r, echo=FALSE}
library(dplyr)
library(GenomicRanges)

load('../RData/utr3_combined_canonical_sites.RData')



```


Need to annotate polyA site by:
1) Having evidence of usage (via polyaDB)
2) The # of the polyA site (1st, 2nd, 3rd) in the transcript
```{r}

# read in polyA site annotations
sites <- data.table::fread('../external_data/clusters_withTissueInfo.bed')
sites$V1 <- gsub(pattern = 'chr', '', sites$V1)

sites$start <- ifelse(sites$V6 == '+',
                      sites$V2 - 55,
                      sites$V3)

sites$end <- ifelse(sites$V6 == '+',
                    sites$V2,
                    sites$V3 + 55)

sites <- makeGRangesFromDataFrame(sites,
                                  keep.extra.columns = TRUE,
                                  starts.in.df.are.0based = TRUE,
                                  seqnames.field = 'V1',
                                  start.field = 'start',
                                  end.field = 'end',
                                  strand.field = 'V6')

# remove full-length UTRs from annotatinos
utr3_polya <- utr3_combined_canonical %>% filter(utr_gquad_start > 0)
utr3_polya$seq <- with(utr3_polya, substr(seq, utr_gquad_start, end.y))

utr3_polya$feature_start <- ifelse(utr3_polya$strand > 0,
                                   utr3_polya$start + utr3_polya$utr_gquad_start - 1,
                                   utr3_polya$start - utr3_polya$utr_gquad_start + 1)

utr3_polya$feature_end <- ifelse(utr3_polya$strand > 0,
                                 utr3_polya$start + utr3_polya$end.y - 1,
                                 utr3_polya$start - utr3_polya$end.y + 1)

utr3_polya$strand <- ifelse(utr3_polya$strand > 0,
                            '+',
                            '-')
utr3_polya$start <- ifelse(utr3_polya$strand == '+',
                           utr3_polya$feature_start,
                           utr3_polya$feature_end)

utr3_polya$end.x <- ifelse(utr3_polya$strand == '-',
                           utr3_polya$feature_end,
                           utr3_polya$feature_start)

utr3_polya_range <- makeGRangesFromDataFrame(utr3_polya, keep.extra.columns = TRUE,seqnames.field = 'chromosome',
                                             start.field = 'start',
                                             end.field = 'end.x',
                                             strand.field = 'strand')
```

overlap ranges to subset polya sites that have evidence of being used
```{r}
used <- subsetByOverlaps(utr3_polya_range, sites, ignore.strand = FALSE)
```

